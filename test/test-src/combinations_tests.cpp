#include <CharCombination.h>

#include <algorithm>

class CharCombinationTester : public ::testing::Test
{
	protected:
	CharCombination tCharCombination;

	void SetUp() override {}
	void TearDown() override {}
};

TEST_F(CharCombinationTester, constructor)
{
	CharCombination tComb("abcd");
	ASSERT_STREQ(tComb.GetCombination().c_str(), "abcd");
}

TEST_F(CharCombinationTester, shorter_that_window)
{
	std::vector<std::string> tVector = CharCombination::GetCombinations("wow");
	ASSERT_EQ(tVector.size(), 0);
	tVector = CharCombination::GetCombinations("wo");
	ASSERT_EQ(tVector.size(), 0);
	tVector = CharCombination::GetCombinations("w");
	ASSERT_EQ(tVector.size(), 0);
	tVector = CharCombination::GetCombinations("");
	ASSERT_EQ(tVector.size(), 0);
}

TEST_F(CharCombinationTester, number_of_combinations)
{
	std::vector<std::string> tVector = CharCombination::GetCombinations("word");
	ASSERT_EQ(tVector.size(), 1);
	tVector = CharCombination::GetCombinations("feels");
	ASSERT_EQ(tVector.size(), 3);
	tVector = CharCombination::GetCombinations("healer");
	ASSERT_EQ(tVector.size(), 6);
	tVector = CharCombination::GetCombinations("acceptance");
	ASSERT_EQ(tVector.size(), 28);
}

TEST_F(CharCombinationTester, proper_combinations)
{
	std::vector<std::string> tResultVector = CharCombination::GetCombinations("words");
	std::vector<std::string> tExpectedVector{"word", "words", "ords"};
	// sort arrays to ensure elements order
	std::sort(tResultVector.begin(), tResultVector.end());
	std::sort(tExpectedVector.begin(), tExpectedVector.end());
	ASSERT_TRUE(tResultVector == tExpectedVector);
	/*
	while(!tResultVector.empty())
	{
		std::string tString = tResultVector.back(); tResultVector.pop_back();
		for(std::string tExpected : tExpectedVector)
		{
			if(tExpected == tString)
				tExpectedVector.
		}
	}
	*/
}

TEST_F(CharCombinationTester, combination_hit)
{
	CharCombination tComb("abcd");
	ASSERT_EQ(tComb.HitCount(), 1);	// on creation combination is hit once
	for(int i = 0; i < 100; ++i)
		tComb.Hit();
	ASSERT_EQ(tComb.HitCount(), 101);
}
