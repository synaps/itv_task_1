#include <CharCombination.h>
#include <ResultFormatter.h>

#include <vector>

#define NUM_HITS 10
class FormatterTester : public ::testing::Test
{
	protected:
	ResultFormatter *tFormatter;

	void SetUp() override 
	{
		CharCombination tC1, tC2, tC3("smth"), tC4("more");
		// writing loops would require more chars :)
		tC3.Hit(); tC3.Hit(); tC3.Hit(); tC3.Hit();
		tC4.Hit(); tC4.Hit();
		std::vector<CharCombination> tVector{tC1, tC2, tC3, tC4};

		tFormatter = new ResultFormatter(tVector, NUM_HITS);
	}


	void TearDown() override 
	{
		delete tFormatter;
	}

	float GetFormatterMaxFrequency(ResultFormatter &pFormatter){ return pFormatter.MaxFrequency; }
	std::vector<CharCombination> GetFormatterVector(ResultFormatter &pFormatter) { return pFormatter.Results; }
	unsigned int GetFormatterLongestComb(ResultFormatter &pFormatter) { return pFormatter.LongestCombination; }
	void SetFormatterTotalHits(ResultFormatter &pFormatter, unsigned int pHits){ pFormatter.TotalHits = pHits; }
	unsigned int GetFormatterTotalHits(ResultFormatter &pFormatter) { return pFormatter.TotalHits; }
	float CalculateFrequency(ResultFormatter &pFormatter, unsigned int pHits){ return pFormatter.CalculateFrequency(pHits); }
	unsigned int GetOffset(ResultFormatter &pFormatter, unsigned int pHits) { return pFormatter.CalculateOffset(pHits); }
};

TEST_F(FormatterTester, constructor)
{
	ASSERT_EQ(GetFormatterMaxFrequency(*tFormatter), 500.0/NUM_HITS);
	ASSERT_EQ(GetFormatterVector(*tFormatter).size(), 4);
	ASSERT_EQ(GetFormatterLongestComb(*tFormatter), 4 + 1);		// additional symbol for spacing
	ASSERT_EQ(GetFormatterTotalHits(*tFormatter), NUM_HITS);
}

TEST_F(FormatterTester, calculate_frequency)
{
	SetFormatterTotalHits(*tFormatter, NUM_HITS);
	ASSERT_EQ(CalculateFrequency(*tFormatter, 1), NUM_HITS);
	SetFormatterTotalHits(*tFormatter, 25);
	ASSERT_EQ(CalculateFrequency(*tFormatter, 5), 500/25.0);
}

TEST_F(FormatterTester, calculate_offset)
{
	ASSERT_EQ(GetFormatterLongestComb(*tFormatter), 5);	// 80 - 5 = 75
	// 75 - NUM_OFFSET = 68
	// 68 - 1 (separator) - 1 (additional space) = 66
	ASSERT_EQ(GetFormatterMaxFrequency(*tFormatter), 500/10);
	// max_freq = 50
	ASSERT_EQ(CalculateFrequency(*tFormatter, 3), 30);
	// current per cent: 30*10/10 = 30%
	// available offset * current % / max %
	ASSERT_EQ(GetOffset(*tFormatter, 3), 66 * 30 / 50);
}
