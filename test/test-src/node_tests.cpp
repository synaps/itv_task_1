#include <Node.h>
#include <CharCombination.h>

class NodeTester : public ::testing::Test
{
	protected:

};

TEST_F(NodeTester, constructor)
{
	std::string tCombination("abcd");
	CharCombination tComb(tCombination);
	Node tRoot(tComb);
	ASSERT_STREQ(tRoot.GetContent().GetCombination().c_str(), tCombination.c_str());
}

TEST_F(NodeTester, tree_contains)
{

}
