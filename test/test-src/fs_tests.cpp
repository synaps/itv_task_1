#include <filesystem>

#include <FileManager.h>

namespace fs = std::filesystem;

class FileManagerTester : public ::testing::Test
{
protected:
	FileManager tFManager;
	fs::path tFilePath = "data1.txt";		// file contains Lorem Ipsum (547 words long)

	void SetUp() override
	{
	}

	void TearDown() override
	{}
};

TEST_F(FileManagerTester, file_read_test)
{
	std::vector<std::string> tWords = tFManager.ReadFile(tFilePath);
	// number of words according to `wc -w` command
	ASSERT_EQ(tWords.size(), 547);
}

TEST_F(FileManagerTester, file_sensible_content)
{
	std::vector<std::string> tWords = tFManager.ReadFile(tFilePath);
	ASSERT_STRNE(tWords.at(0).c_str(), tWords.at(1).c_str());
	ASSERT_STREQ(tWords.at(1).c_str(), tWords.at(2).c_str());
}
