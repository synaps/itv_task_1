#include <gtest/gtest.h>

// including test files
#include <fs_tests.cpp>		// file manager
#include <combinations_tests.cpp>	// char combinations
//#include <node_tests.cpp>		// btree
#include <formatter_tests.cpp>


int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
