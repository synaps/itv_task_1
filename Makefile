all: build

build: build/main.o build/FileManager.o build/CharCombination.o build/ResultFormatter.o
	cd build; g++ -std=c++17 -g main.o FileManager.o CharCombination.o ResultFormatter.o -lstdc++fs -o proekspert

build/main.o: src/main.cpp
	g++ -std=c++17 -Wall -g -Iinclude src/main.cpp -c -o build/main.o

build/FileManager.o: src/FileManager.cpp
	g++ -std=c++17 -Wall -g -Iinclude src/FileManager.cpp -c -o build/FileManager.o

build/CharCombination.o: src/CharCombination.cpp
	g++ -std=c++17 -Wall -g -Iinclude src/CharCombination.cpp -c -o build/CharCombination.o

build/ResultFormatter.o: src/ResultFormatter.cpp
	g++ -std=c++17 -Wall -g -Iinclude src/ResultFormatter.cpp -c -o build/ResultFormatter.o

.PHONY: test
test:
	cd test; make

clean:
	rm -r build/*.o
	rm build/proekspert
