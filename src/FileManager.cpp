#include <fstream>
#include <string>

#include <FileManager.h>

/**
*/
std::vector<std::string> FileManager::ReadFile(std::filesystem::path pFilePath)
{
	std::vector<std::string> rWords;		// declare array to return
	std::ifstream tInputFileStream(pFilePath);
	if(tInputFileStream.is_open())
	{
		std::string tWord;
		while(tInputFileStream >> tWord)
		{
			rWords.push_back(tWord);
		}
		tInputFileStream.close();
	}

	return rWords;
}
