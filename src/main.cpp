#include <FileManager.h>
#include <CharCombination.h>
#include <ResultFormatter.h>

#include <vector>
#include <iostream>
#include <algorithm>


const int RESULTS_TO_DISPLAY = 10;

int main(int argc, char* argv[])
{
	// get words out of the file
	FileManager tFManager;
	std::vector<std::string> tWords = tFManager.ReadFile("./test/data1.txt");
	// exit if file was not read succesfully
	if(tWords.empty())
	{
		std::cout << "File does not contain data. Exiting.\n";
		return 1;
	}
	// construct list of nodes with combinations
	// count all the combinations as well
	std::vector<CharCombination> tCharCombinations;
	unsigned int TotalCombinations = 0;
	for(std::string word : tWords)
	{
		// get combinations from the next word
		std::vector<std::string> tCombinations = CharCombination::GetCombinations(word);
		TotalCombinations += tCombinations.size();
		for(std::string comb : tCombinations)
		{
			// if combination is already in the list hit it
			// othewise add it the list
			CharCombination tComb(comb);
			// locate combination in the list
			auto tCIterator = std::find(tCharCombinations.begin(), tCharCombinations.end(), tComb);
			if(tCIterator != tCharCombinations.end())	// if object exists hit it, otherwise add to the list
				tCIterator->Hit();
			else
				tCharCombinations.push_back(tComb);
		}
	}
	// get 10 top most combinations
	/** Compare unnamed struct: required for std::sort */
	struct 
	{
		bool operator()(CharCombination pA, CharCombination pB) { return pA.HitCount() > pB.HitCount(); }
	} CharCombinationSorter;
	// descending sort by the number of hits
	std::sort(tCharCombinations.begin(), tCharCombinations.end(), CharCombinationSorter);
	// print the table & the graph
	// resizing vector is not the right way as it introduces unnecessary elements if size is larger than initial
	ResultFormatter tFormatter(tCharCombinations, TotalCombinations);
	tFormatter.DisplayResult();
	// finish execution
	return 0;
}
