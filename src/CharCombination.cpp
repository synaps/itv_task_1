#include <CharCombination.h>

CharCombination::CharCombination(std::string pCombination)
{
	if(pCombination.length() >= MIN_CHAR_COMB_LENGTH)
	{
		Combination = pCombination;
	}

	// When combination is created it has already been hit
	Count = 1;
}

/**
*/
std::vector<std::string> CharCombination::GetCombinations(std::string pWord)
{
	std::vector<std::string> rVector;
	// static cast the size as it may be unsigned number on Linux x64
	for(int StartPos = 0; StartPos <= static_cast<int>(pWord.length()) - MIN_CHAR_COMB_LENGTH; ++StartPos)
	{
		for(unsigned int Window = MIN_CHAR_COMB_LENGTH; StartPos + Window <= pWord.length(); ++Window)
		{
			rVector.push_back(pWord.substr(StartPos, Window));
		}
	}

	return rVector;
}

std::string CharCombination::GetCombination() const
{
	return Combination;
}


void CharCombination::Hit()
{
	Count++;
}


unsigned int CharCombination::HitCount() const
{
	return Count;
}


// operators
/** Equals operator that is needed for algorthms functions to find is object exesists in the list */
bool CharCombination::operator==(const CharCombination &pl)
{
	return this->GetCombination() == pl.GetCombination();
}


