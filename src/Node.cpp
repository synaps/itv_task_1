#include <Node.h>

Node::Node(CharCombination pCombination)
{
	Content = pCombination;
	Count = 1;		// When created it was already hit once
	/* not including btree implementation
	Left = nullptr;
	Right = nullptr;
	*/
}

CharCombination Node::GetContent()
{
	return Content;
}

/* not including btree implementation
bool Node::TreeContains(CharCombination pContent)
{
	return false;
}

bool Node::TreeContains(Node *pNode, CharCombination pContent)
{
	return false;
}
*/

void Node::Hit()
{
	Count++;
}

unsigned int Node::HitCount()
{
	return Count;
}
