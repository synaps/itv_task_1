#include <ResultFormatter.h>

#include <ios>
#include <iomanip>


ResultFormatter::ResultFormatter(std::vector<CharCombination> pArray, unsigned int pTotalHits, unsigned int pSize)
{
	if(pArray.size() >= pSize)
	{
		// even though parameter passing is not efficient we are modifying vector
		// don't want this change visible in the caller's context
		pArray.resize(pSize);
		Results = pArray;
	}
	else
		Results = pArray;

	// Least frequent combination is not necesseraly the longest
	unsigned int tLength = Results.front().GetCombination().length();
	for(CharCombination comb : Results)
		if(comb.GetCombination().length() > tLength)
			tLength = comb.GetCombination().length();

	LongestCombination = tLength + 1;	// additional char for spacing
	TotalHits = pTotalHits;
	// We assume sorted array, but that just an assumption
	unsigned int tCount = Results.front().HitCount();
	for(CharCombination comb : Results)
		if(comb.HitCount() > tCount)
			tCount = comb.HitCount();

	MaxFrequency = CalculateFrequency(tCount);
}


float ResultFormatter::CalculateFrequency(unsigned int pHitCount)
{
	return (float)(pHitCount * 100) / TotalHits;
}


void ResultFormatter::PrintChartPart(CharCombination pCombination)
{
	std::cout << std::setfill(' ') << std::right;
	std::cout << std::setw(LongestCombination) << pCombination.GetCombination();
	std::cout << std::setw(NUM_OFFSET) << std::setprecision(4) << CalculateFrequency(pCombination.HitCount()) << "%";
}


/** Good to have length of the line below 80 chars to fit in terminal */
unsigned int ResultFormatter::CalculateOffset(unsigned int pHits)
{
	const unsigned int LineLength = 80;
	// remove additiona char for spacing and separator
	unsigned int rOffset = LineLength - LongestCombination - NUM_OFFSET - 2;
	float tCurrentFreqPercent = CalculateFrequency(pHits);
	rOffset = tCurrentFreqPercent * rOffset / MaxFrequency;
	// sections to fill
	return rOffset;
}


void ResultFormatter::PrintGraphPart(CharCombination pCombination)
{
	std::cout << std::setfill('=') << std::right;
	std::cout << std::setw(CalculateOffset(pCombination.HitCount())) << '*';
}


/** Displays defined in constructor number of character combinations.
*/
void ResultFormatter::DisplayResult()
{
	for(CharCombination comb : Results)
	{
		PrintChartPart(comb);
		std::cout << "|";		// print delimiter
		PrintGraphPart(comb);
		std::cout << std::endl;
//		std::cout << i + 1 << ": " << tCharCombinations[i].GetCombination() << " " << tCharCombinations[i].HitCount() << " " << (float)(tCharCombinations[i].HitCount() * 100) / TotalCombinations << "%" << std::endl;
	}
}
