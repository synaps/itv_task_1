#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include <vector>
#include <filesystem>
#include <string>

//#include <CharCombination.h>

class FileManager
{
	public:
	std::vector<std::string> ReadFile(std::filesystem::path pFilePath);
};

#endif
