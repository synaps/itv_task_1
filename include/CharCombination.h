#ifndef CHARCOMBINATION_H
#define CHARCOMBINATION_H

#include <vector>
#include <string>

#define MIN_CHAR_COMB_LENGTH 4

class CharCombination
{
	private:
	std::string Combination;
	unsigned int Count;
	void SetCombination(std::string pCombination)__attribute__((deprecated));	// use constructor instead
	public:
	CharCombination(std::string pCombination = "---");
	std::string GetCombination() const;
	void Hit();
	unsigned int HitCount() const;
	static std::vector<std::string> GetCombinations(std::string pWord);
	// operators
	bool operator==(const CharCombination &pl);
};

#endif
