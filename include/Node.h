#ifndef NODE_H
#define NODE_H

#include <CharCombination.h>

class Node
{
	private:
	CharCombination Content;
	unsigned int Count;		// number of times appeared
	/* not including btree implementation
	Node *Left;
	Node *Right;
	*/

	public:
	Node(CharCombination pCombination);
	//~Node();		//TODO: implement Node destructor
	CharCombination GetContent();
	/* not including btree implementation
	bool TreeContains(CharCombination pContent);
	static bool TreeContains(Node *pNode, CharCombination pContent);
	*/
	void Hit();
	unsigned int HitCount();
};

#endif
