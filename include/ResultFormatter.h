#ifndef RESULT_FORMATTER_H
#define RESULT_FORMATTER_H

#include <CharCombination.h>

#include <vector>
#include <iostream>

class ResultFormatter
{
	private:
	// For testing of private members. May be placed with macro defenition.
	friend class FormatterTester;

	float MaxFrequency;		// Maximum value of frequency (for scaling the graph line)
	unsigned int TotalHits;
	std::vector<CharCombination> Results;	// Storing the results
	unsigned int LongestCombination;		// Length of the longest combination

	// precision = 4, '%', decimal dot, additional space
	const unsigned int NUM_OFFSET = 7;
	
	float CalculateFrequency(unsigned int pHitCount);
	void PrintChartPart(CharCombination pCombination);
	unsigned int CalculateOffset(unsigned int pHits);
	void PrintGraphPart(CharCombination pCombination);

	public:
	ResultFormatter(std::vector<CharCombination> pArray, unsigned int pTotalHits, unsigned int pSize = 10);
	void DisplayResult();
};

#endif
